package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Utils {
    //variables
    WebDriver driver;
    String url;

    //method public boolean will return a true or false. Example - public string would return a string.
    public boolean navigateToWebsite (WebDriver driver) {
        this.driver = driver;
        url = "http://automate.safebear.co.uk/";
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }
}
